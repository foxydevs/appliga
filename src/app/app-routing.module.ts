import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArbitroGuard } from './_guard/arbitro.guard';
import { AuthGuard } from './_guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule'},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule', canActivate: [ArbitroGuard]},
  { path: 'arbitro', loadChildren: './arbitro/arbitro.module#ArbitroPageModule', canActivate: [AuthGuard] },
  { path: '**', redirectTo: '', pathMatch: 'full' },  { path: 'detalle-partido-arbitro', loadChildren: './arbitro/detalle-partido-arbitro/detalle-partido-arbitro.module#DetallePartidoArbitroPageModule' },
  { path: 'modal-jugador', loadChildren: './arbitro/modal-jugador/modal-jugador.module#ModalJugadorPageModule' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
