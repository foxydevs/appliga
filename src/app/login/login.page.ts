import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from "@angular/router";
import { ControladoresIonicService } from '../_services/controladores-ionic.service';
import { AuthService } from '../_services/auth.service';
import { Location } from '../../../node_modules/@angular/common';

declare var $: any

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  auth:any
  titulo="Iniciar Sesión"
  closeResult: string;
  pictureLogin:string = 'http://s1.picswalls.com/wallpapers/2014/07/25/beautiful-football-wallpaper_041157178_96.jpg';
  pictureApp:string = 'https://scontent.fgua1-1.fna.fbcdn.net/v/t1.0-9/35238987_855412347992699_5349088214743252992_n.png?_nc_cat=109&_nc_ht=scontent.fgua1-1.fna&oh=c32838381daed5fdde7c1a56238e574c&oe=5C78E58C';
  user:any = {
    username:'',
    password: ''
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public navCtrl: NavController,
    //private _service: NotificationsService,
    public mainService: AuthService,
    public messagesService: ControladoresIonicService,
    public location: Location
  ) { }

  public options = {
    position: ["bottom", "right"],
    timeOut: 3000,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    lastOnBottom: false,
    preventDuplicates: true,
    animate: "scale",
    maxLength: 400
  };

  goToRoute(route:string) {
    this.navCtrl.goForward(route);
  }
  goToBack() {
    this.location.back();
  }

  logIn(){
    this.mainService.authentication(this.user)
    .then(response => {
      console.log(response)
      localStorage.setItem('currentTorneos', response[0]);
      localStorage.setItem('currentUser', response.username);
      localStorage.setItem('currentId', response.id);
      if(response.arbitros) {
        localStorage.setItem('currentIdArbitro', response.arbitros.id);
        localStorage.setItem('currentFirstName', response.arbitros.nombre);
        localStorage.setItem('currentLastName', response.arbitros.apellido);
      }
      localStorage.setItem('currentPicture', response.picture);
      localStorage.setItem('currentEmail', response.email);
      localStorage.setItem('currentState', response.estado);
      this.goToRoute('arbitro');
    }).catch(error => {
      console.log(error)
      this.messagesService.presentToast('Usuario o contraseña incorrectos.');
    })
        /*//console.log(`user: ${formValue.username} pass: ${formValue.password}`)
        console.log(this.user.username)
        console.log('entro');
        
        let type:string = null;
        console.clear

    console.log('salio');*/
    
  }

  ngOnInit() {
  }

}
