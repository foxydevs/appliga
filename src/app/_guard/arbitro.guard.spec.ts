import { TestBed, async, inject } from '@angular/core/testing';

import { ArbitroGuard } from './arbitro.guard';

describe('ArbitroGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArbitroGuard]
    });
  });

  it('should ...', inject([ArbitroGuard], (guard: ArbitroGuard) => {
    expect(guard).toBeTruthy();
  }));
});
