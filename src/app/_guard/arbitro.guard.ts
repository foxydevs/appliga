import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class ArbitroGuard implements CanActivate {

    constructor(
      private router: Router,
      public navCtrl: NavController
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (localStorage.getItem('currentTorneos')) {
        this.navCtrl.goForward('arbitro');
      }

      if (!localStorage.getItem('currentTorneos')) {
        return true;
      }

    }
}
