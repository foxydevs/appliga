import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbitroPage } from './arbitro.page';

describe('ArbitroPage', () => {
  let component: ArbitroPage;
  let fixture: ComponentFixture<ArbitroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbitroPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbitroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
