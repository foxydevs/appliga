import { Component, OnInit } from '@angular/core';
import { NavController } from '../../../node_modules/@ionic/angular';
import { ControladoresIonicService } from '../_services/controladores-ionic.service';
import { ArbitroService } from '../_services/arbitro.service';

@Component({
  selector: 'app-arbitro',
  templateUrl: './arbitro.page.html',
  styleUrls: ['./arbitro.page.scss'],
})
export class ArbitroPage implements OnInit {
  titulo = 'arbitro';
  Table:any = [];
  idArbitro:any = localStorage.getItem('currentIdArbitro');
  constructor(private navCtrl: NavController,
  private controladorService:ControladoresIonicService,
  private mainServices: ArbitroService) { }

  ngOnInit() {
    this.getMatches(this.idArbitro)
  }

  //LOG OUT
  logOut() {
    localStorage.clear();
    this.goToRoute('home');
  }

  goToRoute(route:string) {
    this.navCtrl.goForward(route);
  }

  getMatches(id){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getMatches(id)
    .then(response=>{
      this.Table = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }
}
