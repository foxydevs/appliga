import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
import { PartidosService } from '../../_services/partidos.service';
import { Location } from '../../../../node_modules/@angular/common';
import { JugadoresService } from '../../_services/jugadores.service';
import { ModalController } from '../../../../node_modules/@ionic/angular';
import { ModalJugadorPage } from '../modal-jugador/modal-jugador.page';

@Component({
  selector: 'app-detalle-partido-arbitro',
  templateUrl: './detalle-partido-arbitro.page.html',
  styleUrls: ['./detalle-partido-arbitro.page.scss'],
})
export class DetallePartidoArbitroPage implements OnInit {
  titulo = 'Detalle del Partido';
  Table;
  jugadores;
  parameter: any;
  selectItem: any = '1';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    private modalController: ModalController,
    private controladorService: ControladoresIonicService,
    private mainServices: PartidosService,
    private secondService: JugadoresService,
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.parameter = this.route.snapshot.paramMap.get('id');
    this.getSingle(this.parameter);
  }

  getSingle(id:any){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getSingle(id)
    .then(response=>{
      console.log(response)
      this.Table = response;
      this.getSingleSecond(response.equipos[0].equipos.id)
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  getSingleSecond(id:any){
    this.controladorService.present("Cargando Jugadores");
    this.secondService.getByTeam(id)
    .then(response=>{
      console.log(response)
      this.jugadores = [];
      this.jugadores = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  async presentModal(parameter:any, equipo:any) {
    const modal = await this.modalController.create({
      component: ModalJugadorPage,
      componentProps: { value: parameter, match: this.parameter, team: equipo }
    });
    modal.onDidDismiss(() => {
      this.getSingle(this.parameter);
    });
    return await modal.present();
  }
}
