import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePartidoArbitroPage } from './detalle-partido-arbitro.page';

describe('DetallePartidoArbitroPage', () => {
  let component: DetallePartidoArbitroPage;
  let fixture: ComponentFixture<DetallePartidoArbitroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallePartidoArbitroPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePartidoArbitroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
