import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetallePartidoArbitroPage } from './detalle-partido-arbitro.page';

const routes: Routes = [
  {
    path: '',
    component: DetallePartidoArbitroPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetallePartidoArbitroPage]
})
export class DetallePartidoArbitroPageModule {}
