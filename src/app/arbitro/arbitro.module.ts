import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArbitroPage } from './arbitro.page';
import { ArbitroService } from '../_services/arbitro.service';
import { DetallePartidoArbitroPage } from './detalle-partido-arbitro/detalle-partido-arbitro.page';
import { PartidosService } from '../_services/partidos.service';
import { JugadoresService } from '../_services/jugadores.service';
import { ModalJugadorPage } from './modal-jugador/modal-jugador.page';

const routes: Routes = [
  {
    path: '',
    component: ArbitroPage
  },
  {
    path: 'detalle-partido/:id',
    component: DetallePartidoArbitroPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArbitroPage,
    DetallePartidoArbitroPage,
    ModalJugadorPage,
  ],
  entryComponents:[
    DetallePartidoArbitroPage,
    ModalJugadorPage,
  ],
  providers: [
    ArbitroService,
    PartidosService,
    JugadoresService
  ]
})
export class ArbitroPageModule {}
