import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '../../../../node_modules/@ionic/angular';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { JugadoresService } from '../../_services/jugadores.service';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';

@Component({
  selector: 'app-modal-jugador',
  templateUrl: './modal-jugador.page.html',
  styleUrls: ['./modal-jugador.page.scss'],
})
export class ModalJugadorPage implements OnInit {
  titulo = 'Jugador'
  parameter:any;
  idPartido:any;
  idTeam:any;
  Table;
  data = {
    cantidad: 0,
    jugador: '',
    partido: '',
    equipo: ''
  }
  data2 = {
    cantidad: 0,
    jugador: '',
    partido: '',
    equipo: ''
  }
  btnDisabled:boolean = false;
  btnDisabled2:boolean = false;
  constructor(private modalController:ModalController,
  private navParams: NavParams,
  private mainServices: JugadoresService,
  private controladorService: ControladoresIonicService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.idPartido = this.navParams.get('match');
    this.idTeam = this.navParams.get('team');
    console.log(this.idTeam)
    this.data.jugador = this.parameter;
    this.data.partido = this.idPartido;
    this.data.equipo = this.idTeam;

    this.data2.jugador = this.parameter;
    this.data2.partido = this.idPartido;
    this.data2.equipo = this.idTeam;
    this.getSingle(this.parameter);
  }
  
  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  getSingle(id){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getSingle(id)
    .then(response=>{
      this.Table = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  //AGREGAR
  saveChanges() {
    this.btnDisabled = true;
    this.mainServices.createGoal(this.data)
    .then(response => {
      this.controladorService.confirmation('Cambios Guardados', 'Los cambios se guardaron exitosamente.');
      this.closeModal();
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //AGREGAR
  saveChanges2() {
    this.btnDisabled2 = true;
    this.mainServices.createCard(this.data2)
    .then(response => {
      this.controladorService.confirmation('Cambios Guardados', 'Los cambios se guardaron exitosamente.');
      this.closeModal();
    }).catch(error => {
      this.btnDisabled2 = false;
      console.clear
    });
  }

  add() {
    this.data.cantidad = this.data.cantidad + 1;
  }

  remove() {
    if(this.data.cantidad > 0) {
      this.data.cantidad = this.data.cantidad - 1;
    }
  }

  add2() {
    this.data2.cantidad = this.data2.cantidad + 1;
  }

  remove2() {
    if(this.data2.cantidad > 0) {
      this.data2.cantidad = this.data2.cantidad - 1;
    }
  }

}
