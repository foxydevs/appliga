import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalJugadorPage } from './modal-jugador.page';

describe('ModalJugadorPage', () => {
  let component: ModalJugadorPage;
  let fixture: ComponentFixture<ModalJugadorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalJugadorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalJugadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
