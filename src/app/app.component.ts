import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '../../node_modules/@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  auth:any = localStorage.getItem('currentTorneos');
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`]);
    document.querySelector('ion-menu-controller')
    .open('end');
  }

  //LOG OUT
  logOut() {
    localStorage.clear();
    this.auth = localStorage.getItem('currentTorneos');
    this.goToRoute('home');
  }
}
