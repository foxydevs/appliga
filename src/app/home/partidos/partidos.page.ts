import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
import { PartidosService } from '../../_services/partidos.service';
import { ModalController } from '@ionic/angular';
import { DetallePartidoComponent } from '../detalle-partido/detalle-partido.component';
@Component({
  selector: 'app-partidos',
  templateUrl: './partidos.page.html',
  styleUrls: ['./partidos.page.scss'],
})
export class PartidosPage implements OnInit {
  titulo:string = "Partidos"
  Table:any = [];
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };
  idArbitro:any = localStorage.getItem('currentIdArbitro');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    private controladorService: ControladoresIonicService,
    private mainServices: PartidosService,
    private modalController: ModalController
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getAll()
    .then(response=>{
      this.Table = response;
      this.Table.reverse();
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  
  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: DetallePartidoComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }

}
