import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { EquiposService } from "./../../_services/equipos.service";
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.page.html',
  styleUrls: ['./equipos.page.scss'],
})
export class EquiposPage implements OnInit {
  titulo:string = "Equipos"
  Table: any;
  selectedData: any;
  private search: any;
  currentId:any = '';
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };
  idArbitro:any = localStorage.getItem('currentIdArbitro');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    public controladorService: ControladoresIonicService,
    private mainServices: EquiposService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    
    this.route.params
              .switchMap((params: Params) => (params['id']))
              .subscribe(response => { 
                                this.currentId+=response
                            });
    this.getAll(this.currentId);
  }

  getAll(id){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getAll()
                      .then(response=>{
                        this.Table = response;
                        this.controladorService.dismiss();
                      }).catch(error=>{
                        console.log(error);
                        this.controladorService.dismiss();
                      })
  }

  openStart() {
    document.querySelector('ion-menu-controller')
      .open('start');
  }

}
