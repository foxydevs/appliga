import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
import { PartidosService } from '../../_services/partidos.service';
import * as moment from 'moment';

@Component({
  selector: 'app-detalle-partido',
  templateUrl: './detalle-partido.component.html',
  styleUrls: ['./detalle-partido.component.scss']
})
export class DetallePartidoComponent implements OnInit {
  parameter:any;
  data:any;
  titulo:any = 'Detalle del Partido';
  fecha:any;
  constructor(
    public navParams: NavParams,
    public messagesService: ControladoresIonicService,
    public mainService: PartidosService,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.getSingleCliente(this.parameter)
  }

  //GET SINGLE
  public getSingleCliente(id:any){
    this.messagesService.present('Cargando...');
    this.mainService.getSingle(id)
    .then(response => {
      this.data = response;
      this.fecha = moment(response.fecha).format('LL');
      this.messagesService.dismiss();
    }).catch(error => {
      this.messagesService.dismiss();
      console.clear;
    })
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

}
