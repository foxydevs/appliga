import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichajesPage } from './fichajes.page';

describe('FichajesPage', () => {
  let component: FichajesPage;
  let fixture: ComponentFixture<FichajesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichajesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichajesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
