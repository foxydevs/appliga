import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
@Component({
  selector: 'app-envivo',
  templateUrl: './envivo.page.html',
  styleUrls: ['./envivo.page.scss'],
})
export class EnvivoPage implements OnInit {
  titulo:string = "En Vivo";
  idArbitro:any = localStorage.getItem('currentIdArbitro');
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
  }

  openStart() {
    document.querySelector('ion-menu-controller')
      .open('start');
  }

}
