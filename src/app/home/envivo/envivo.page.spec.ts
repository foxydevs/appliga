import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvivoPage } from './envivo.page';

describe('EnvivoPage', () => {
  let component: EnvivoPage;
  let fixture: ComponentFixture<EnvivoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvivoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvivoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
