import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { NoticiasService } from "./../_services/noticias.service";
import { ControladoresIonicService } from '../_services/controladores-ionic.service';
import { DetalleNoticiaComponent } from './detalle-noticia/detalle-noticia.component';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo:string = "Home"
  Table: any;
  selectedData: any;
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };

  constructor(
    public navCtrl: NavController,
    private mainServices: NoticiasService,
    private controladorService: ControladoresIonicService,
    private modalController: ModalController
  ) { }

  goToRoute(route:string) {
    this.navCtrl.goForward(route);
  }
  ngOnInit() {
    //this.getAll();
    this.getAllNews();
  }
  getAll(){
    this.Table = [
      {
        descripcion:"Final copa A",
        fecha:"4 de junio",
        lugar:"Estadio zona 6",
        foto:null
      },
      {
        descripcion:"Academia de vacaciones",
        fecha:"24 de noviembre",
        lugar:"Inscripcion Abierta",
        foto:null
      },
      {
        descripcion:"Amistosos",
        fecha:"Titulos",
        lugar:"vs",
        foto:null
      }
    ]
    // this.mainServices.getAll()
    //                   .then(response=>{
    //                     console.log(response);
                        
    //                   }).catch(error=>{
    //                     console.log(error);
                        
    //                   })
  }

  getAllNews(){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getAll()
    .then(response=>{
      this.Table = response;
      this.Table.reverse();
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: DetalleNoticiaComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }

}
