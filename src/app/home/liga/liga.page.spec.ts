import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LigaPage } from './liga.page';

describe('LigaPage', () => {
  let component: LigaPage;
  let fixture: ComponentFixture<LigaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LigaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LigaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
