import { Component, OnInit } from '@angular/core';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
import { PartidosService } from '../../_services/partidos.service';
import { NavParams, ModalController } from '../../../../node_modules/@ionic/angular';
import * as moment from 'moment';
import { NoticiasService } from '../../_services/noticias.service';

declare var google;

@Component({
  selector: 'app-detalle-noticia',
  templateUrl: './detalle-noticia.component.html',
  styleUrls: ['./detalle-noticia.component.scss']
})
export class DetalleNoticiaComponent implements OnInit {
  parameter:any;
  map: any;
  data:any;
  titulo:any = 'Detalle Noticia';
  fecha:any;
  constructor(
    public navParams: NavParams,
    public messagesService: ControladoresIonicService,
    public mainService: NoticiasService,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.getSingle(this.parameter)
  }

  //GET SINGLE
  public getSingle(id:any){
    this.messagesService.present('Cargando...');
    this.mainService.getSingle(id)
    .then(response => {
      this.data = response;
      this.fecha = moment(response.date).format('LL');
      this.loadMap(response.latitude, response.longitude);
      this.messagesService.dismiss();
    }).catch(error => {
      this.messagesService.dismiss();
      console.clear;
    })
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  public loadMap(lat:any, long:any){
  let latitude = lat;
  let longitude = long;
  
    let mapEle: HTMLElement = document.getElementById('map');

    let myLatLng = {lat: latitude, lng: longitude};

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        title: 'Hello World!'
      });
      mapEle.classList.add('show-map');
    });
  }


}
