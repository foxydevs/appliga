import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { Location } from '../../../../node_modules/@angular/common';
import { LoadingController } from '../../../../node_modules/@ionic/angular';
import { LigasService } from '../../_services/ligas.service';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';

@Component({
  selector: 'app-ligas',
  templateUrl: './ligas.page.html',
  styleUrls: ['./ligas.page.scss'],
})
export class LigasPage implements OnInit {
  titulo:string = "Ligas"
  Table: any;
  selectedData: any;
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };
  idArbitro:any = localStorage.getItem('currentIdArbitro');

  constructor(public router: Router,
  public loadingController: LoadingController,
  public controladorService: ControladoresIonicService,
  public location:Location,
  public mainServices: LigasService)
  { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  getAll(){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getAll()
    .then(response=>{
      this.Table = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  openStart() {
    document.querySelector('ion-menu-controller')
      .open('start');
  }

}
