import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeEquipoPage } from './mensaje-equipo.page';

describe('MensajeEquipoPage', () => {
  let component: MensajeEquipoPage;
  let fixture: ComponentFixture<MensajeEquipoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeEquipoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeEquipoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
