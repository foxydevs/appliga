import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { NoticiasService } from "./../../_services/noticias.service";
@Component({
  selector: 'app-amistosos',
  templateUrl: './amistosos.page.html',
  styleUrls: ['./amistosos.page.scss'],
})
export class AmistososPage implements OnInit {
  titulo:string = "Amistosos"
  Table: any;
  selectedData: any;
  private search: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    private mainServices: NoticiasService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    this.Table = [
      {
        nombre:"Mayor A",
        id:1,
      },
      {
        nombre:"Mayor B",
        id:2,
      },
      {
        nombre:"Mayor C",
        id:3,
      },
      {
        nombre:"Mayor D",
        id:4,
      },
    ]
    // this.mainServices.getAll()
    //                   .then(response=>{
    //                     console.log(response);
                        
    //                   }).catch(error=>{
    //                     console.log(error);
                        
    //                   })
  }

}
