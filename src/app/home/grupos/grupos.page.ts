import { Component, OnInit } from '@angular/core';
import { Location } from '../../../../node_modules/@angular/common';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
import { GruposService } from '../../_services/grupos.service';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.page.html',
  styleUrls: ['./grupos.page.scss'],
})
export class GruposPage implements OnInit {
  titulo:string = "Grupos"
  Table: any;
  selectedData: any;
  parameter: any;
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };
  idArbitro:any = localStorage.getItem('currentIdArbitro');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    public controladorService: ControladoresIonicService,
    private mainServices: GruposService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  
  ngOnInit() {
    this.parameter = this.route.snapshot.paramMap.get('id');
    this.getAll(this.parameter);
  }

  getAll(id){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getGroupsOfTournaments(id)
    .then(response=>{
      this.Table = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }


}
