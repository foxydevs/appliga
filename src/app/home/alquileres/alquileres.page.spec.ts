import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlquileresPage } from './alquileres.page';

describe('AlquileresPage', () => {
  let component: AlquileresPage;
  let fixture: ComponentFixture<AlquileresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlquileresPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlquileresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
