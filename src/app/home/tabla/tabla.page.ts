import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { Location } from '../../../../node_modules/@angular/common';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
import { EquiposService } from '../../_services/equipos.service';
import { PartidosService } from '../../_services/partidos.service';
import { GruposService } from '../../_services/grupos.service';
import { ModalController } from '@ionic/angular';
import { DetallePartidoComponent } from '../detalle-partido/detalle-partido.component';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.page.html',
  styleUrls: ['./tabla.page.scss'],
})
export class TablaPage implements OnInit {
  titulo:string = "Tabla de Posiciones"
  Table: any;
  Table2: any;
  data1: any;
  selectedData: any;
  parameter: any;
  selectItem:any = 'tabla';
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };
  idArbitro:any = localStorage.getItem('currentIdArbitro');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    private controladorService: ControladoresIonicService,
    private mainServices: GruposService,
    private secondServices: PartidosService,
    private modalController: ModalController
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.parameter = this.route.snapshot.paramMap.get('id');
    this.getAll(this.parameter);
    this.getAllMatches();
  }

  getAll(id){
    this.controladorService.present("Cargando Equipos");
    this.mainServices.getTeamsOfGroups(id)
    .then(response=>{
      this.Table = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  getAllMatches(){
    this.controladorService.present("Cargando Partidos");
    this.secondServices.getAll()
    .then(response=>{
      this.Table2 = response;
      this.Table2.reverse();
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: DetallePartidoComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }

}
