import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';
import { JugadoresService } from '../../_services/jugadores.service';

@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.page.html',
  styleUrls: ['./jugador.page.scss'],
})
export class JugadorPage implements OnInit {
  titulo:string = "Jugador"
  parameter:any;
  data:any = [];
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    }
  };
  idArbitro:any = localStorage.getItem('currentIdArbitro');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    private controladorService: ControladoresIonicService,
    private mainServices: JugadoresService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.parameter = this.route.snapshot.paramMap.get('id');
    this.getSingle(this.parameter);
  }

  getSingle(id){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getSingle(id)
    .then(response=>{
      this.data = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }
}
