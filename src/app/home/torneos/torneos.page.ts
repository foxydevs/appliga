import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController } from '@ionic/angular';
import { LigasService } from '../../_services/ligas.service';
import { ControladoresIonicService } from '../../_services/controladores-ionic.service';

@Component({
  selector: 'app-torneos',
  templateUrl: './torneos.page.html',
  styleUrls: ['./torneos.page.scss'],
})
export class TorneosPage implements OnInit {
  titulo:string = "Torneos"
  Table: any;
  selectedData: any;
  parameter: any;
  idArbitro:any = localStorage.getItem('currentIdArbitro');

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    public loadingController: LoadingController,
    public controladorService: ControladoresIonicService,
    private mainServices: LigasService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.parameter = this.route.snapshot.paramMap.get('id');
    this.getAll(this.parameter);
  }

  getAll(id){
    this.controladorService.present("Cargando "+this.titulo);
    this.mainServices.getTournaments(id)
    .then(response=>{
      this.Table = response;
      this.controladorService.dismiss();
    }).catch(error=>{
      console.log(error);
      this.controladorService.dismiss();
    })
  }

}
