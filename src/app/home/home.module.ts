import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { NoticiasService } from './../_services/noticias.service';
import { JugadoresService } from './../_services/jugadores.service';
import { TorneosService } from './../_services/torneos.service';
import { EquiposService } from './../_services/equipos.service';
import { PartidosService } from './../_services/partidos.service';

import { HomePage } from './home.page';
import { TorneosPage } from "./torneos/torneos.page";
import { AlquileresPage } from "./alquileres/alquileres.page";
import { MenuPage } from "./menu/menu.page";
import { AmistososPage } from "./amistosos/amistosos.page";
import { FichajesPage } from "./fichajes/fichajes.page";
import { ChatPage } from "./chat/chat.page";
import { PartidosPage } from "./partidos/partidos.page";
import { EnvivoPage } from "./envivo/envivo.page";
import { EquiposPage } from "./equipos/equipos.page";
import { EquipoPage } from "./equipo/equipo.page";
import { LigaPage } from "./liga/liga.page";
import { JugadorPage } from "./jugador/jugador.page";
import { MensajesPage } from "./mensajes/mensajes.page";
import { MensajeEquipoPage } from "./mensaje-equipo/mensaje-equipo.page";
import { LigasPage } from './ligas/ligas.page';
import { LigasService } from '../_services/ligas.service';
import { GruposPage } from './grupos/grupos.page';
import { GruposService } from '../_services/grupos.service';
import { TablaPage } from './tabla/tabla.page';
import { DetallePartidoComponent } from './detalle-partido/detalle-partido.component';
import { DetalleNoticiaComponent } from './detalle-noticia/detalle-noticia.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {
        path: 'torneos',
        component: TorneosPage
      },
      {
        path: 'alquileres',
        component: AlquileresPage
      },
      {
        path: 'amistosos',
        component: AmistososPage
      },
      {
        path: 'fichajes',
        component: FichajesPage
      },
      {
        path: 'chat',
        component: ChatPage
      },
      {
        path: 'partidos',
        component: PartidosPage
      },
      {
        path: 'envivo',
        component: EnvivoPage
      },
      {
        path: 'liga/:id',
        component: LigaPage
      },
      {
        path: 'equipos/:id',
        component: EquiposPage
      },
      {
        path: 'equipo/:id',
        component: EquipoPage
      },
      {
        path: 'jugador/:id',
        component: JugadorPage
      },
      {
        path: 'chat/mensaje/:id',
        component: MensajesPage
      },
      {
        path: 'equipo/:id/mensaje',
        component: MensajeEquipoPage
      },
      {
        path: 'ligas',
        component: LigasPage
      },
      {
        path: 'torneos/:id',
        component: TorneosPage
      },
      {
        path: 'grupos/:id',
        component: GruposPage
      },
      {
        path: 'tabla/:id',
        component: TablaPage
      }
    ])
  ],
  declarations: [
    HomePage,
    TorneosPage,
    AlquileresPage,
    MenuPage,
    AmistososPage,
    FichajesPage,
    ChatPage,
    MensajesPage,
    EnvivoPage,
    PartidosPage,
    EquiposPage,
    MensajeEquipoPage,
    LigaPage,
    JugadorPage,
    EquipoPage,
    LigasPage,
    GruposPage,
    TablaPage,
    DetallePartidoComponent,
    DetalleNoticiaComponent,
  ],
  entryComponents: [
    TorneosPage,
    AlquileresPage,
    MenuPage,
    AmistososPage,
    FichajesPage,
    ChatPage,
    MensajesPage,
    EnvivoPage,
    PartidosPage,
    EquiposPage,
    MensajeEquipoPage,
    LigaPage,
    JugadorPage,
    EquipoPage,
    GruposPage,
    TablaPage,
    DetallePartidoComponent,
    DetalleNoticiaComponent,
  ],
  providers: [
    NoticiasService,
    JugadoresService,
    TorneosService,
    PartidosService,
    EquiposService,
    LigasService,
    GruposService
  ]
})
export class HomePageModule {}
