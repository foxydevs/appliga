import { Injectable } from '@angular/core';
import { LoadingController, ToastController, AlertController } from '../../../node_modules/@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ControladoresIonicService {
  //PROPIEDADES 
  isLoading = false;

  constructor(public loadingController: LoadingController,
  public toastController: ToastController,
  public alertController: AlertController
  ) { }

  async present(msg:string) {
    this.isLoading = true;
    return await this.loadingController.create({
      content: msg
    }).then((a) => {
      a.present().then(() => {
        //console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => {
            //console.log('abort presenting')
          });
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => {
      //console.log('dismissed')
    });
  }

  async presentToast(msg:any) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1000
    });
    toast.present();
  }

  async confirmation(title:any, description:any) {
    const alert = await this.alertController.create({
      header: title,
      message: description,
      buttons: ['OK']
    });

    await alert.present();
  }
}
