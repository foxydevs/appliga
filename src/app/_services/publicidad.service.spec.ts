import { TestBed, inject } from '@angular/core/testing';

import { PublicidadService } from './publicidad.service';

describe('PublicidadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PublicidadService]
    });
  });

  it('should be created', inject([PublicidadService], (service: PublicidadService) => {
    expect(service).toBeTruthy();
  }));
});
