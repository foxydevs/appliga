import { TestBed, inject } from '@angular/core/testing';

import { ControladoresIonicService } from './controladores-ionic.service';

describe('ControladoresIonicService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ControladoresIonicService]
    });
  });

  it('should be created', inject([ControladoresIonicService], (service: ControladoresIonicService) => {
    expect(service).toBeTruthy();
  }));
});
