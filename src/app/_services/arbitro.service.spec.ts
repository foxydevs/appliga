import { TestBed, inject } from '@angular/core/testing';

import { ArbitroService } from './arbitro.service';

describe('ArbitroService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArbitroService]
    });
  });

  it('should be created', inject([ArbitroService], (service: ArbitroService) => {
    expect(service).toBeTruthy();
  }));
});
