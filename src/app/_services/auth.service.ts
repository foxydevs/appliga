import { Injectable } from '@angular/core';
import { path } from '../config.module';
import { Http } from '../../../node_modules/@angular/http';
import "rxjs/add/operator/toPromise";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http: Http) { }

  //HANDLE ERROR
  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //AUTENTICACIÓN
  public authentication(login:any):Promise<any> {
    let url = `${this.basePath}/api/login`
    return this.http.post(url,login)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }
}
