# Torneos

Este proyecto fue generado con [IONIC CLI](https://ionicframework.com/docs/cli/) version 4.0 Beta.

## Development server

Corre el comando `ionic serve --no-open` para iniciar un servidor de desarrollo en la direccion `http://localhost:8100/` el comando `-p` permite cambiar puerto. La aplicación se recargará automaticamente si haces algun cambio en el codigo .

## Code scaffolding

Corre el comando `ng generate page namePage` para generar una Pagina nueva. Tambien puedes usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Corre el comando `ionic cordova build [plataform]` para hacer build al proyecto puedes hacer build para `Android` e `IOS`. La App final se alojara en la carpeta `plataforms/`. Use la opcion `-prod` para buildear una version de producción.

## Running unit tests

Corre el comando `ng test` para ejecutar las pruebas unitarias con [Karma](https://karma-runner.github.io).

## Backend Configuration

Para configurar el Backend del proyecto debe copiar el archivo `src/app/config.module.ts.example` como `src/app/config.module.ts` y agregar la direccion del Backend
